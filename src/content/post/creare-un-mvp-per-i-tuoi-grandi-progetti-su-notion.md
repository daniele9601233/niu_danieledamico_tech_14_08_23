---
publishDate: 2023-01-12T00:00:00Z
title: Creare un MVP per i tuoi grandi progetti su Notion
image: https://images.unsplash.com/photo-1516996087931-5ae405802f9f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80
category: Tutorials
tags:  
    - astro  
    - tailwind css
metadata:  
    canonical: https://astrowind.vercel.app/
---

Ehi ciao 👋,

questo è il numero 372 del **Kit di Sopravvivenza per Creator**.

E oggi ti parlerò dell'MVP dei grandi progetti, su Notion.

**TL;DR**

- Creare un MVP per i tuoi grandi progetti, su Notion!
- Una risorsa per la tua audience building;
- Una risorsa per la tua produttività;
- Una risorsa per la tua monetization.

Il tempo di lettura previsto è: 4 minuti e 30

Ok, tutti devono avere obiettivi nella vita.

È una cosa ovvia.

C'è chi preferisce avere obiettivi a distanza di un anno, chi a due anni e chi di più.

Io però ho sempre avuto un problema sulla questione.

Ho sempre avuto difficoltà a capire se un obiettivo poteva rendermi soddisfatto per davvero.

Sarà una paranoia mia, ma quando mi metto a fare i piani del futuro, mi ritrovo sempre a dire una cosa del genere.

"Ok, ora mi metto e mi impegno per i prossimi 4-5 mesi per raggiungere questo obiettivo.

Ma se poi mi rendo conto che l'obiettivo non mi rende felice?"

## **L'MVP DEI GRANDI PROGETTI, SU NOTION**

In tal caso, succede che hai sprecato tempo.

E ok, magari hai imparato cose, ma il risultato è comunque uno spreco.

Quindi che si fa?

E che c'entra Notion?

- Tim Ferriss, gli esperimenti di due settimane e i progetti di 6 mesi
- Come creare il tuo MVP su Notion;
- E dopo l'esperimento?

Come avrai intuito, ora te lo spiego.

### **TIM FERRISS, GLI ESPERIMENTI DI DUE SETTIMANE E I PROGETTI DI 6 MESI**

Per anni il dubbio è rimasto nella mia testa.

Poi ho sentito un [podcast di Tim Ferriss](https://podclips.com/c/LacN4G) e ho capito una cosa fondamentale!

Ho capito che puoi fare **esperimenti anche con i tuoi progetti a lungo termine**.

Esperimenti in cui provi una versione ridotta del tuo obiettivo a lungo termine, senza dedicarci mesi ma solo un paio di settimane.

Così, alla fine delle due settimane, se la cosa ti è piaciuta e hai ottenuto risultati rilevanti, allora puoi passare all'obiettivo a lungo termine.

Altrimenti, cambi esperimento o cambi obiettivo.

In pratica, **un MVP per gli obiettivi di vita**.

E la cosa mi ha fatto scoppiare il cervello.

### **COME CREARE IL TUO MVP SU NOTION**

E dato che ho le mie fisse, ho preparato un **sistema per costruire questi MVP su Notion**.

Come ci sono riuscito?

- Ho usato il database Area
- Ho usato il database Goals
- Ho creato il database Sprint/Esperimenti

Il primo database, quello "Area", è un semplice database in cui inserisco le **aree principali in cui suddivido la mia vita**.

In particolare, le aree sono: Lavoro, Relazioni e Salute.

Poi, è il turno del secondo database, quello chiamato Goals.

In questo database, inserisco tutti gli obiettivi che vorrei raggiungere a lungo termine.

Cerco di avere un **limite massimo di 3 obiettivi a lungo termine per area**.

Infine, è il turno del database Sprint/Esperimenti.

E questo è il contro principale di questo piccolo sistema.

All'interno di questo database, inserisco le date di inizio e di fine dell'esperimento e altre proprietà utili (se l'esperimento è attivo, quali sono i criteri di successo ecc).

Infine, creo due proprietà.

Una **Relation** al database Goals (che uso per collegare l'esperimento all'obiettivo a lungo termine di cui è un MVP) e poi una proprietà **Rollup** che mostra, in automatico, l'area a cui questo esperimento appartiene.

### **E DOPO L'ESPERIMENTO?**

Così, ogni volta che mi salta in mente un nuovo obiettivo a lungo termine, penso prima al modo in cui lo posso sperimentare.

E questo cambia tutto.

Perché così, ogni obiettivo a lungo termine porta in sé fin dall'inizio, una **maggiore possibilità di successo**.

Non è più un semplice obiettivo nell'etere, è qualcosa di pratico fin dall'inizio.

E c'è un piano, ci sono dei task, l'investimento di tempo è minimo.

Insomma, per me è un sistema molto efficace.

Ora, non dico che per te possa rivelarsi utile allo stesso modo, eh.

Ma credo che, se hai incontrato le mie stesse difficoltà in passato, allora dovresti farlo un tentativo.

Consideralo un esperimento, che dici?

Capita la battuta?

E niente, direi che anche per oggi è tutto, buona settimana e **beccati le risorse**.

Daniele

## **👥 UNA RISORSA PER FARE AUDIENCE BUILDING**

Sei alla ricerca di una nicchia nuova da aiutare?

[Questa potrebbe fare al caso tuo, non ne parla ancora nessuno ma è in esplosione.](https://www.ramoswriter.com/rising-finance-phenomenon/)

## **🚀 UNA RISORSA PER LA TUA PRODUTTIVITÀ**

Quando si parla di produttività, scommetto che pensi a risparmiare tempo.

O almeno, a utilizzarlo in modo più efficace.

[E in tal caso, da una newsletter che secondo me dovresti seguire, questo articolo ti potrebbe aiutare proprio a raggiungere questo obiettivo.](https://emiliodalbo.substack.com/p/4-abitudini-da-1-minuto-per-salvare)

## **💸 UNA RISORSA PER LA TUA MONETIZATION**

L'hub centrale di tutti i tuoi tentativi di monetization?

L'homepage del tuo sito (o con un po' di sforzo, il profilo del social che più utilizzi).

[Ma come costruirla al meglio?](https://twitter.com/jspector/status/1673044526221631488)