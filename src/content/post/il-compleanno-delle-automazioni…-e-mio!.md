---
publishDate: 2023-01-12T00:00:00Z
title: Il compleanno delle automazioni… e mio!
image: https://images.unsplash.com/photo-1516996087931-5ae405802f9f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80
category: Tutorials
tags:  
    - astro  
    - tailwind css
metadata:  
    canonical: https://astrowind.vercel.app/
---

Ehi ciao 👋,

questo è il numero 379 del **Kit di Sopravvivenza per Creator**.

E oggi svelerò 3 automazioni che ho preparato in vacanza.

**TL;DR**

- 3 automazioni per i vacanzieri
- Crea contenuti con i bias cognitivi;
- Come valutare i tuoi appunti;
- 10 modi per aumentare le vendite;

**Il tempo di lettura previsto è**: 4 minuti

Lo so che in vacanza ci si dovrebbe riposare.

Si dovrebbe spegnere il cervello e pensare ad altro.

Solo che dopo qualche giorno di “forzata” noia, proprio non sono riuscito a stare fermo.

Avevo dei problemi da risolvere.

E le automazioni mi potevano aiutare.

Quindi ho aperto Make.com e mi sono messo a lavoro.

E oggi ti mostrerò i risultati di questo mio periodo di ferie.

- Task da Obsidian a Notion
- Idee da Obsidian a Notion
- Newsletter Notion al sito web

Ci sei?

Cominciamo subito!

## IDEE DA OBSIDIAN A NOTION

Ormai sono due mesi che mi faccio una bella camminata non appena mi sveglio.

Tutti i giorni.

Non è per dimagrire, o stare in salute.

Lo faccio perché camminare mi permette di avere un sacco di idee.

Il cervello attiva la sua parte più creativa e nella mia testa cominciano a emergere decine e decine di idee.

Alcune riguardo [Notion Builders](https://www.notionbuilders.it/), altre riguardo questa newsletter e altre riguardo altri progetti.

Solo che c’è un **problema**.

Quando hai così tante idee è importante che tu le salvi da qualche parte.

E nel mio caso, dopo mesi passati a testare diverse soluzioni, ho deciso che le dovevo salvare su **Notion**.

E su **Obsidian**.

Non ti spiegherò il motivo di questa divisione del mio Second Brain, se vuoi [puoi leggere qui qualche dettaglio in più](https://danieledamico.tech/tags/e-obsidian-come-lo-uso/).

Oggi mi limito a dirti come faccio a **far finire le idee da Obsidian su Notion**.

Perché questo è il modo migliore per sfruttare i punti di forza di entrambi gli strumenti.

E come faccio?

- Ho scaricato il plug-in di Obsidian “**Ultimate Todoist Sync**” che ti permette di sincronizzare **Obsidian** e **Todoist**;
- Quando ho un’idea, apro Obsidian e la scrivo (come task), assegnandole il tag "Todoist" e anche il tag “idea”;
- L’idea poi finisce su Todoist nel progetto “Idee”;
- Parte l’automazione che ho creato su Make.com;
- Ogni volta che un nuovo task compare nel mio account Todoist, Make lo salva in un relativo database su Notion, a seconda del progetto di cui fa parte;
- Se è nel **progetto Idee**, l’automazione salva l’informazione nel database Idee del mio spazio personale di Notion.

Lo so, è un po’ intricata ma ti posso garantire che funziona alla grande!

## TASK DA OBSIDIAN A NOTION

Questa automazione è complementare a quella del punto precedente.

Perché su Obsidian non inserisco solo le idee, inserisco anche i **task**.

E voglio che comunque siano presenti su **Notion**, perché è lì che **lavoro e gestisco i miei progetti**.

Quindi, inserisco un task su Obsidian nelle stesse modalità del punto precedente, solo che in questo caso non gli assegno il tag Idee ma uno relativo al progetto di cui fa parte.

Se è un **task personale**, il tag è “Personal” e se invece è un task di **Notion Builders**, il tag è “Notion Builders”.

Così quando poi parte l’automazione di Make, questa trova il tag e il task e li salva negli appositi database su Notion.

## **NEWSLETTER DA NOTION AL SITO WEB!**

Questa è l’automazione più recente.

L’obiettivo è quello di poter **pubblicare i miei articoli di newsletter sul mio sito web**.

Ogni settimana scrivo la newsletter su Obsidian, poi la copio a mano su Convertkit e poi a mano sul mio sito web.

Insomma, una situazione in cui un’**automazione sarebbe ideale**.

E dopo vari tentativi, finalmente ho trovato la soluzione.

Un’automazione che mi permette di pubblicare la newsletter sul mio sito web, direttamente da Notion.

Anche in questo caso ci pensa Make.com.

Parte ogni volta che compare un nuovo numero di newsletter all’interno dello specifico database su Notion.

Prende il contenuto di quella newsletter, crea un file all’interno della repository del mio sito web su Gitlab e lo carica.

Poi l’automazione dice a Netlify di aggiornare il sito e mandarlo in produzione.

E alla fine, giusto perché non mi faccio mancare nulla, prende quella stessa newsletter e la pubblica anche su **Medium**.

Così non devo affidarmi a Webflow o Framer (che costano un bel po’), posso continuare a divertirmi col codice.

E soprattutto non spreco tempo.

Non vedo l’ora di attivare l’automazione, quando il mio **nuovo sito web sarà online**!

Ora, lo so che non sono sceso nei dettagli di come funzionano queste automazioni.

Ma diciamoci la verità, non era quello l’obiettivo.

Oggi volevo solo mostrarti come le automazioni ti possano aiutare a **gestire problemi pratici**.

Nei prossimi numeri di questa newsletter (e non solo qui) scenderò più in dettaglio e magari ti mostrerò **tutorial dettagliati**.

E per oggi è tutto, buona settimana e beccati le risorse.

Daniele

PS:

Comunque oggi è il mio compleanno e ti faccio un regalo.

**Uno sconto del 40%** su tutti i [miei template](https://danieledamico.gumroad.com/).

Ti basta inserire il codice "30-compleanno" per accedere allo sconto, ma affrettati che hai 48 ore di tempo.

Ti lascio il pulsante per Easy Second Brain già con lo sconto applicato.

https://danieledamico.gumroad.com/l/easy-second-brain/30-compleanno

## **👥 UNA RISORSA PER FARE AUDIENCE BUILDING**

Sai cosa sono i bias cognitivi?

Penso proprio di si.

[Ma sai che puoi utilizzarli anche nei tuoi contenuti?](https://www.thestealclub.com/blog/4-psychology-hacks)

## **🚀 UNA RISORSA PER LA TUA PRODUTTIVITÀ**

In che stato sono le tue note?

[Ecco un modo per valutarle al meglio, anche se sono note su taccuini di carta!](https://howaboutthis.substack.com/p/creative-realizer-a-notebook-harvest)

## **💸 UNA RISORSA PER LA TUA MONETIZATION**

Hai una newsletter (anche se non è fondamentale) ?

[Ecco 10 modi per aumentare le vendite!](https://joshspector.com/how-to-boost-sales/)